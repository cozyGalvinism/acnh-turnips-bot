import os
import importlib
import asyncio
import aiohttp
from discord.ext import commands

from utils import conf, schedule, log

CONF = conf.get()
LOG = log.get('discord')

# setup command prefix and help settings
bot = commands.Bot(command_prefix=CONF['cmd_prefix'])
bot.pm_help = CONF['pm_help']

# start scheduler
schedule.start(bot)

# https://stackoverflow.com/a/50981577
def handle_exit():
    LOG.critical("handling bot exit")
    # logout properly
    bot.loop.run_until_complete(bot.logout())
    # set schedule as stopped
    schedule._STARTED.clear()
    # cancel all pending tasks
    for t in asyncio.Task.all_tasks(loop=bot.loop):
        if t.done():
            t.exception()
            continue
        t.cancel()
        try:
            # wait for a task to complete in the next 5 seconds before shutdown
            bot.loop.run_until_complete(asyncio.wait_for(t, 5, loop=bot.loop))
        except (asyncio.InvalidStateError, asyncio.TimeoutError, asyncio.CancelledError):
            pass

ON_READY_HANDLERS = []
# import all modules
for mod in os.listdir('modules'):
    # modules are only valid if they're regular .py files
    if mod[0] == '_' or mod[-3:] != '.py':
        continue

    module = 'modules.{}'.format(mod[:-3])
    bot.load_extension(module)

    module = importlib.import_module(module)
    try:
        ON_READY_HANDLERS.append(module.on_ready)
    except AttributeError:
        pass

while True:
    try:
        LOG.info("starting bot")
        bot.run(CONF['token'])
    except (aiohttp.ClientResponseError, ConnectionResetError):
        LOG.critical("CONNECTION RESET, RESTARTING BOT")
        handle_exit()
    except (SystemExit, KeyboardInterrupt):
        LOG.info("Bot is shutting down...")
        handle_exit()
        LOG.info("Closing loop")
        bot.loop.close()
        LOG.info("Bot stopped.")
        break

    LOG.warning("Bot is restarting...")
    bot = commands.Bot(command_prefix=CONF['cmd_prefix'])
    bot.pm_help = CONF['pm_help']
    schedule.start(bot)
