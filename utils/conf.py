import json
import logging
from pytz import timezone
from pytz.exceptions import UnknownTimeZoneError

_loglevel = {
    'critical': logging.CRITICAL,
    'error': logging.ERROR,
    'warning': logging.WARNING,
    'info': logging.INFO,
    'debug': logging.DEBUG
}

DEFAULTS = {
    "token": "__INVALID__",
    "cmd_prefix": "a!",
    "db_folder": "storage", 
    "pm_help": False,
    "timezone": "Etc/UTC",
    "loglevel": "info"
}

_CACHE = {}

def get(file="config"):
    # add .json file extension if missing
    if file[-5:] != '.json':
        file += '.json'

    if file not in _CACHE:
        with open(file, 'r') as f:
            conf = json.load(f)

            # add missing keys from defaults
            for k, v in DEFAULTS.items():
                if k not in conf:
                    conf[k] = v

            # convert timezone
            try:
                conf['timezone'] = timezone(conf['timezone'])
            except UnknownTimeZoneError:
                conf['timezone'] = timezone('Etc/UTC')

            # convert loglevel
            try:
                conf['loglevel'] = _loglevel[conf['loglevel'].lower()]
            except KeyError:
                conf['loglevel'] = logging.INFO

            _CACHE[file] = conf

    return _CACHE[file]