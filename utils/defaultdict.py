class DefaultDict(dict):

    def __init__(self, default, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.default = default

    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except KeyError:
            return self.default[key]
        