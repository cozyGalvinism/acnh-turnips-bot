import logging
import os

from . import conf

CONF = conf.get()

_logs = {}

def get(name):
    if not os.path.exists('logs'):
        os.makedirs('logs')

    if name not in _logs:    
        logger = logging.getLogger(name)
        logger.setLevel(CONF['loglevel'])
        handler = logging.FileHandler(filename='logs/{}.log'.format(name), encoding='utf-8', mode='w')
        handler.setFormatter(logging.Formatter('[%(asctime)s][%(levelname)s][%(name)s]: %(message)s'))
        logger.addHandler(handler)

        _logs[name] = logger

    return _logs[name]

    
