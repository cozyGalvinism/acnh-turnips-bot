from peewee import *
from playhouse.migrate import SqliteMigrator, migrate
import datetime

def pattern_migration(database: SqliteDatabase):
    migrator = SqliteMigrator(database)
    last_pattern = IntegerField(default=-1)
    current_pattern = IntegerField(default=-1)
    pattern_valid_until = DateField(default=datetime.datetime.now)

    migrate(
        migrator.add_column('person', 'last_pattern', last_pattern),
        migrator.add_column('person', 'current_pattern', current_pattern),
        migrator.add_column('person', 'pattern_valid_until', pattern_valid_until)
    )