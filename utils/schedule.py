import sched
import time
import asyncio

from . import log
from threading import Event
from pytz import UTC
from datetime import datetime


def get_time():
    dt = UTC.localize(datetime.utcnow())
    return dt.timestamp()


SCHEDULER = sched.scheduler(get_time, time.sleep)
LOG = log.get('scheduler')
_STARTED = Event()


class SchedulerNotStarted(Exception):
    pass


def start(bot):
    if not _STARTED.is_set():
        # loop running in background
        async def backgroud_task():
            await bot.wait_until_ready()

            # run while bot is online
            while not bot.is_closed():
                # get the time of next event and run pending events
                next_event = SCHEDULER.run(False)
                if next_event is not None:
                    LOG.debug('Next event in {}'.format(next_event))
                    await asyncio.sleep(min(1, next_event))
                else:
                    await asyncio.sleep(1)

        bot.loop.create_task(backgroud_task())
        _STARTED.set()


def add(bot, what, when, absolute=True, args=[], kwargs={}):
    if not _STARTED.is_set():
        raise SchedulerNotStarted()

    def run(*args, **kwargs):
        asyncio.run_coroutine_threadsafe(what(*args, **kwargs), bot.loop)

    if absolute:
        return SCHEDULER.enterabs(when, 1, run, argument=args, kwargs=kwargs)
    else:
        return SCHEDULER.enter(when, 1, run, argument=args, kwargs=kwargs)


def now(bot, what, args=[], kwargs={}):
    return add(bot, what, 1, absolute=False, args=args, kwargs=kwargs)

def cancel(event):
    if not _STARTED.is_set():
        raise SchedulerNotStarted()

    try:
        SCHEDULER.cancel(event)
    except ValueError:
        pass
