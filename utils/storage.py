import os

from uuid import uuid1
from base64 import b64encode
from peewee import SqliteDatabase, Model, IntegerField, TimestampField, TextField, CompositeKey

from . import conf

CONF = conf.get()
# db objects
_dbs = {}


def generateID(node=133780085, prefix='', altchars=b'##'):
    uuid = uuid1(node=node)
    # get the first 64 bits
    i = uuid.int >> 64

    # get int as bytes
    b = i.to_bytes((i.bit_length() + 7) // 8, 'big')

    # encode in base64 with '#' as the instead of '+' and '/' and strip '='
    s = b64encode(b, altchars=altchars).decode().rstrip('=')

    return '{!s}.{!s}'.format(prefix, s)


def prefixedIDGenerator(prefix, node=133780085, altchars=b'##'):
    def wrapped():
        return generateID(node=node, prefix=prefix, altchars=altchars)
    return wrapped


def getDatabase(db) -> SqliteDatabase:
    # create db folder
    if not os.path.exists(CONF['db_folder']):
        os.makedirs(CONF['db_folder'])

    # force lowercase and strip '.db' extension
    db = db.lower()
    if db[-3:] == '.db':
        db = db[:-3]

    try:
        return _dbs[db]
    except KeyError:
        _dbs[db] = SqliteDatabase(os.path.join(CONF['db_folder'], db + '.db'))
        return _dbs[db]


def get_version_model(db):
    class Version(Model):
        major = IntegerField(null=False)
        minor = IntegerField(null=False)
        point = IntegerField(null=False)
        date = TimestampField(utc=True)
        comment = TextField(null=True)

        class Meta:
            database = db
            primary_key = CompositeKey('major', 'minor', 'point')

    return Version


def get_version(db):
    v = get_version_model(db)
    db.connect()

    db.create_tables([v])

    versions = v.select()
    version = None
    if not versions:
        version = v.create(major=0, minor=0, point=0, comment="Auto-generated")
    else:
        version = versions[-1]

    db.close()
    return version


def set_version(db, version_str, comment=None):
    major, minor, point = version_str.split('.')
    major, minor, point = int(major), int(minor), int(point)

    v = get_version_model(db)

    v.create(major=major, minor=minor, point=point, comment=comment)


def get_version_str(db):
    ver = get_version(db)
    return '{0.major}.{0.minor}.{0.point}'.format(ver)
